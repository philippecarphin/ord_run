// Package tail defines a method for tailing files that are being written to by jobs
package tail

import (
	"bufio"
	"io"
	"log"
	"os"
	"time"
)

// Tail is a basic function for doing the equivalent of 'tail -f'.
// The function will consult the variable '*done' to know when to
// stop and close the file.
func Tail(filename string, done *bool, out io.Writer) {

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	rd := bufio.NewReader(f)
	for {
		line, err := rd.ReadString('\n')
		if err == io.EOF {
			if *done {
				break
			}
			time.Sleep(time.Second * 1)
		} else {
			out.Write([]byte(line))
		}

	}
}

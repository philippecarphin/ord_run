package completion

import (
	"strings"

	"github.com/c-bata/go-prompt"
	"gitlab.science.gc.ca/phc001/ord_run/ordparams"
)

// GetOrdSoumetCompletions returns a slice of prompt.Suggest of possible completions from a prompt.Document.
func GetOrdSoumetCompletions(d prompt.Document) []prompt.Suggest {
	words := strings.Split(d.CurrentLine(), " ")
	return CompleteOrdSoumet(words)
}

// CompleteOrdSoumet returns a slice of prompt.Suggest of possible completions from a list of words.
func CompleteOrdSoumet(words []string) []prompt.Suggest {

	curWord := words[len(words)-1]
	if strings.HasPrefix(words[len(words)-1], "-") {
		return prompt.FilterFuzzy(ordparams.OrdsoumetOptions, curWord, true)
	}
	if strings.HasPrefix(curWord, "=") {
		return []prompt.Suggest{{Text: "VALUE", Description: "Argument for job (will be forwarded without the leading '=')"}}
	}
	for _, w := range words {
		if w == "--" {
			return []prompt.Suggest{{Text: "VALUE", Description: "Argument for job"}}
		}
	}

	if len(words) > 1 {
		optionToComplete := words[len(words)-2]
		if ok := ordparams.OptionValues[optionToComplete]; ok != nil {
			candidates := ordparams.OptionValues[optionToComplete]
			return prompt.FilterHasPrefix(candidates, curWord, true)
		} else {
			return valueSuggestion(optionToComplete)

		}
	}
	return nil
}

func valueSuggestion(optionToComplete string) []prompt.Suggest {
	for _, v := range ordparams.OrdsoumetOptions {
		if v.Text == optionToComplete {
			return []prompt.Suggest{
				{Text: strings.ToUpper(v.Text[1:]), Description: v.Description},
			}
		}
	}
	return nil
}

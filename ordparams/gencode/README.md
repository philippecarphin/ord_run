# Code generation for `ord_soumet` arguments

This package generates go code defining the parameters of ord_soumet and their values.

## Preparing input

The input of the python script must be JSON where the keys are the parameters (things that start with `'-'`) and the values are the descriptions of the parameters.

This is done by taking the help which has lines of the form `INPUT Wrapdir [/home/phc001/.ord_soumet.d/wrap:] job wrapper directories` with extra stuff at the start and at the end.

```bash
ordsoumet --help > ordhelp.txt 2>&1
```

I remove the extra stuff in vim and do this `sed` command.

```bash
# Change first part to openquote
sed 's/INPUT\w+/"/' \
# Change middle part to close quote, colon, openquote (": ")
| sed 's/ /": "/' \
# Add close quote comma
| sed 's/$/",/' 
```

I do these `sed` commands in vim with `:s/.../.../`.

## Generated code

The python script produces two files

One for the gitlab runner and one for the documentation page it generates.

The generated files should be tracked by git.

## Driving generation

Generation can be driven by

```bash
go generate
```

run from this directory.  It uses the `\\go:genrate python3 ...` at the top of ordsoumetarg.go to get the command to run the code genration script.

Node that there is no make-style dependency checking by go build at build time.  The go developers decided that this step should be handled manually.

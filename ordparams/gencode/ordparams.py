import json
import io
import sys
import os

this_file = os.path.realpath(__file__)
this_dir = os.path.dirname(this_file)

source_def = f"{this_dir}/ordargsdef.json"
with open(source_def) as f:
    argsdef = json.load(f)

# OrdSoumetArg defines a class for genrating code related to specifications
# of a genaral ord_soumet argument.
class OrdSoumetArg:
    def __init__(self, name, desc):
        self.name = name
        self.desc = desc

    def write_go_to(self, sb:io.StringIO): # -> str 
        goargname = self.name
        yaml_name = f'ORD_SOUMET_{self.name[1:].upper()}'
        lines = [
            f'\t"{yaml_name}":' + ' {',
            f'\t\tDescription: "{self.desc}",',
            f'\t\tText: "{goargname}",',
            f'\t\tName: "{goargname[1:]}",',
            f'\t\tHasArgs: true,',
            f'\t\tYamlName: "{yaml_name}",',
            '\t},\n',
        ]
        sb.writelines('\n'.join(lines))

    def from_dict(self):
        pass


def write_package_header(writer):
    writer.write("package ordparams\n\n")
    writer.write("// !!!!!!!!!!!!!!!!!!!!!!!!!!!!\n")
    writer.write("// GENERATED CODE DO NOT MODIFY\n")
    writer.write("// !!!!!!!!!!!!!!!!!!!!!!!!!!!!\n")
    writer.write("// See OrdSoumetArgs.go and gencode/ordparams.py\n\n")

def gencode_go_OrdArgList(writer):
    write_package_header(writer)
    writer.write("// OrdParams defines a lookup table taking gitlab-ci.yml variable\n")
    writer.write("// names and returning an OrdSoumetArg.  The ord_soumet executor constructs\n")
    writer.write("// a list of OrdSoumetArg to pass to this package to construct an ord_soumet command.\n")
    writer.write("var OrdParams = map[string]OrdSoumetArg{\n")
    for key, value in argsdef.items():
        arg = OrdSoumetArg(name=key, desc=value)
        arg.write_go_to(writer)
    writer.write('}\n')

def gencode_go_OrdResourceStruct(writer):
    write_package_header(writer)
    writer.write("// OrdSoumetResources is a struct that I made while I was at it generating\n")
    writer.write("// OrdSoumetArgs.go.  It does give a cool help page for ord_soumet though.\n")
    writer.write("type OrdSoumetResources struct {\n")
    for dash_attrib_name, description in argsdef.items():
        # Remove leading '-' and capitalize first letter
        attrib_name = dash_attrib_name[1].upper() + dash_attrib_name[2:]
        writer.write(f'\t// {attrib_name} {description}\n')
        writer.write(f'\t{attrib_name} string\n')
    writer.write("}")
    
if __name__ == "__main__":
    with open(f"{this_dir}/../ordargs.go", 'w') as f:
        gencode_go_OrdArgList(f)
    with open(f"{this_dir}/../ordargs_struct.go", 'w') as f:
        gencode_go_OrdResourceStruct(f)





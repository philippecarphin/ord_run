// Package ordparams defines things related to specifying the arguments of ord_soumet
package ordparams

//go:generate python3 gencode/ordparams.py

// OrdSoumetArg is a struct inspired from c-bata/go-prompt Prompt.Suggest but with
// some extra attributes.  It is used to represent a parameter and its value.
type OrdSoumetArg struct {
	// Value is the argument, the value of the parameter
	Value string
	// Text is the name of the parameter (it starts with a '-') and is used as the source for autocomplete suggestions.
	Text string
	// Description is the right part in the help text of ord_soumet --help.  It is shown next to autocompletion suggestions.
	Description string
	// HasArgs determines whether this option requires an argument.  This controls the autocomplete logic.
	HasArgs bool
	// Name is the text of the parameter like Text[1:].  It feels redundant to have Text and Name but I don't know which one I want to keep.
	Name string
	// YamlName is the name of the variable in a .gitlab-ci.yml file to control this argument.
	YamlName string
}

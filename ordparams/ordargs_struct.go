package ordparams

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!
// GENERATED CODE DO NOT MODIFY
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!
// See OrdSoumetArgs.go and gencode/ordparams.py

// OrdSoumetResources is a struct that I made while I was at it generating
// OrdSoumetArgs.go.  It does give a cool help page for ord_soumet though.
type OrdSoumetResources struct {
	// Addstep [:addstep] add co-scheduled step
	Addstep string
	// Altcfgdir [:] alternate config dir
	Altcfgdir string
	// Args [:] arguments for job script
	Args string
	// As [:] submit job as another user
	As string
	// C [1:1] same as cpus
	C string
	// Clone [0:100] max number of clones (0=none) 
	Clone string
	// Cm [:] memory (K/M/G bytes)
	Cm string
	// Coschedule [no:yes] coscheduled job
	Coschedule string
	// Cpus [:] processes(MxN) and cpus/process(O) MxNxO 
	Cpus string
	// Custom [:] custom parameter for sys config
	Custom string
	// D [eccc-ppp4:eccc-ppp4] synonym for mach 
	D string
	// Display [:] X windows display
	Display string
	// E [:e] controls -e flag
	E string
	// Epilog [:] job epilog
	Epilog string
	// Firststep [:lajob_steps] sum of previous job steps
	Firststep string
	// Geom [:AUTO] MPI geometry file
	Geom string
	// Image [:] OS image to run job (if supported)
	Image string
	// Immediate [:immediate] do not batch, use ssh with batch environment
	Immediate string
	// Iojob [1:5] IO weight (0-9) 0=none, 9=IO hog
	Iojob string
	// Jn [:] job name
	Jn string
	// Jobcfg [:] job configuration commands
	Jobcfg string
	// Jobfile [/dev/null:] name of file to submit
	Jobfile string
	// Jobtar [lajob:lajob] name of tar file from nosubmit
	Jobtar string
	// Keep [:keep] keep job and script file at end of run
	Keep string
	// L [no:yes] job already has wrappers
	L string
	// Laststep [:laststep] last co-scheduled step
	Laststep string
	// Listing [/home/phc001/listings:/home/phc001/tmp]  directory for listings 
	Listing string
	// M [:] same as cm
	M string
	// Mach [:]  target machine 
	Mach string
	// Mail [:philippe.carphin2@canada.ca] email address
	Mail string
	// Mpi [:mpi]  MPI job 
	Mpi string
	// Node [:] job addressing
	Node string
	// Noendwrap [y:n] job end signal not required
	Noendwrap string
	// Norerun [yes:yes] declare that the job is not rerunnable
	Norerun string
	// Norset [no:yes] do not use cpu resource sets for task binding (LoadLeveler only)
	Norset string
	// Nosubmit [:nosubmit]  do not submit 
	Nosubmit string
	// Notify [error:complete] 
	Notify string
	// O [:] same as args
	O string
	// Op [:oper] job is operational flag
	Op string
	// P [:mpi] same as mpi
	P string
	// Postfix [.out:none] listing postfix
	Postfix string
	// Ppid [680634:680634] 
	Ppid string
	// Preempt [:1] allow job to be preempted
	Preempt string
	// Prefix [NoNe:MaChInEcLaSs] listing prefix
	Prefix string
	// Prio [:] batch system specific job priority
	Prio string
	// Project [:] batch system specific project
	Project string
	// Prolog [:] job prolog
	Prolog string
	// Q [:] same as queue
	Q string
	// Queue [:] specify a specific queue/class
	Queue string
	// Rerun [no:yes] declare that the job is rerunnable
	Rerun string
	// Resid [:] LoadLeveler reservation id
	Resid string
	// Rsrc [:] set of needed resources
	Rsrc string
	// Retries [3:3] set of maximum number of retries
	Retries string
	// Seqno [1:1] sequence number of first job (clones) 
	Seqno string
	// Share [:s] can share node and/or be split across nodes e|s|p
	Share string
	// Shell [/bin/ksh:] job shell for batch job
	Shell string
	// Smt [1:2] smt factor
	Smt string
	// Splitstd [n:y] split stderr/stdout in listings
	Splitstd string
	// Sq [:] alternate queue for submission
	Sq string
	// Ssmuse [:] add extra environment
	Ssmuse string
	// Step [de_680634:] job step name
	Step string
	// Sys [batch:batch] system mode
	Sys string
	// T [:] job execution time (seconds)
	T string
	// Tag [:] job tracking tag
	Tag string
	// Threads [1:1] number of threads per cpu(sge only)
	Threads string
	// Tmpfs [:255] fast temporary space (MB)
	Tmpfs string
	// V [:1] verbose
	V string
	// W [1:1] same as t but in mins
	W string
	// Waste [:100] acceptable percentage of wasted cpus
	Waste string
	// With [:] batch subsystem to use (GridEngine LoadLeveler UnixBatch)
	With string
	// Wrapdir [/home/phc001/.ord_soumet.d/wrap:] job wrapper directories
	Wrapdir string
	// Xterm [:xterm] start an xterm in job
	Xterm string
}
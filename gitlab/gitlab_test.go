package gitlab

import (
	"fmt"
	"strings"
	"testing"
)

func TestGitlab(t *testing.T) {
	script := "#!/bin/bash\nhostname\nwhoami"
	rd := strings.NewReader(script)
	sb := strings.Builder{}
	DecorateScript(rd, &sb)
	fmt.Println(sb.String())
}

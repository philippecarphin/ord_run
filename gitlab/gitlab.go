// Package gitlab immitates how gitlab allows us to see our commands in yellow in the CI interface.
package gitlab

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

// DecorateScript takes lines and adds colored echos of those lines
func DecorateScript(in io.Reader, out *strings.Builder) string {
	// out := strings.Builder{}
	bufIn := bufio.NewReader(in)
	pwd, _ := os.Getwd()
	out.WriteString(fmt.Sprintf("cd %s ; ", pwd))
	for {
		lineB, err := bufIn.ReadString('\n')
		if err == io.EOF {
			break
		}
		line := strings.TrimSpace(string(lineB))
		if strings.HasPrefix(line, "#") {
			out.WriteString(echoComment(string(line)) + "\n")
		} else if line != "" {
			out.WriteString(echoCommand(string(line)) + " ; " + string(line) + "\n")
		}
	}
	return out.String()
}

func escape(command string) string {
	// Regex is one of a double quote, a backslash or a dollar sign
	// so the replacement will add a backslash in front of each of
	// these three characters
	re, _ := regexp.Compile("([\"\\\\\\$])")
	return string(re.ReplaceAll([]byte(command), []byte("\\$1")))
}

func echoCommand(command string) string {
	return fmt.Sprintf("echo \"\x1b[32;1m$ %s \x1b[0;m\"", escape(command))
}

func echoComment(command string) string {
	return fmt.Sprintf("echo \"\x1b[32;1m$\x1b[34m %s \x1b[0;m\"", command)
}

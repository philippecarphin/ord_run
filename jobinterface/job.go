// Package jobinterface defines the interface for jobs sent to a queing system
package jobinterface

type IJob interface {
	GetJobExitCode() (int, error)

	Start() error
	// Run() error

	QueueStatus() (string, error)
	// Wait() error

	Id() (string, error)

	Delete() error
}

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/c-bata/go-prompt"
	"gitlab.science.gc.ca/phc001/ord_run/completion"
	"gitlab.science.gc.ca/phc001/ord_run/executor"
	"gitlab.science.gc.ca/phc001/ord_run/gitlab"
)

var file string = ""

// setupSigintHandler gives a channel that will be writen to when the runtime receives a sigint.
func setupSigintHandler() chan os.Signal {
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, syscall.SIGINT)
	return signalCh
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("ERROR : Please provide a filename on the command line")
		os.Exit(1)
	}
	file = os.Args[1]
	fgGreen := prompt.OptionPrefixTextColor(prompt.Green)
	for {
		ps := fmt.Sprintf("ord_soumet %s ", file)
		soumetArgs := prompt.Input(ps, completion.GetOrdSoumetCompletions, fgGreen)
		if soumetArgs == "exit" || soumetArgs == "" {
			break
		}
		jobstatus, err := runCommand(soumetArgs)
		if err != nil {
			fmt.Println(err)
		} else {
			os.Exit(jobstatus)
		}
	}
}

func runCommand(soumetStringArgs string) (int, error) {
	fmt.Printf("Running 'ord_run %s %s'\n", os.Args[1], soumetStringArgs)
	signalCh := setupSigintHandler()

	codeToRun, err := getDecoratedUserFile()
	if err != nil {
		return 0, err
	}

	waitCh, cmd := startCommandWithExecutor(codeToRun, soumetStringArgs)
	go func() {
		exitCode, _ := cmd.Run()
		waitCh <- exitCode
	}()

	var jobStatus int
	select {
	case <-signalCh:
		cmd.AbortCh <- 8
		jobStatus = <-waitCh
	case jobStatus = <-waitCh:
	}

	return jobStatus, nil
}

func getDecoratedUserFile() (string, error) {
	userCode, err := ioutil.ReadFile(os.Args[1])
	sb := strings.Builder{}
	gitlab.DecorateScript(strings.NewReader(string(userCode)+"\n"), &sb)
	if err != nil {
		return "", err
	}
	return sb.String(), nil
}
func startCommandWithExecutor(codeToRun string, soumetStringArgs string) (chan int, *executor.ExecutorCommand) {
	cmd := executor.NewExecutorCommand(codeToRun, nil)
	cmd.SoumetStringArgs = soumetStringArgs
	cmd.KeepTmpFiles = false
	cmd.BuildTrace = os.Stdout

	return make(chan int), cmd
}

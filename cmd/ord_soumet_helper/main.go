package main

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/c-bata/go-prompt"
	"gitlab.science.gc.ca/phc001/ord_run/completion"
)

var file string = ""

func main() {

	if len(os.Args) < 2 {
		fmt.Println("ERROR : Please provide a filename on the command line")
		os.Exit(1)
	}
	file = os.Args[1]
	fgGreen := prompt.OptionPrefixTextColor(prompt.Green)
	for {
		ps := fmt.Sprintf("ord_soumet %s ", file)
		t := prompt.Input(ps, completion.GetOrdSoumetCompletions, fgGreen)
		if t == "exit" || t == "" {
			break
		}
		runCommand(ps + " " + t)
	}
}

func runCommand(command string) {
	fmt.Printf("Running 'bash -c \"%s\"\n", command)
	c := exec.Command("bash", "-c", command)
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	c.Run()
}

package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.science.gc.ca/phc001/ord_run/gitlab"
)

func main() {
	sb := &strings.Builder{}
	gitlab.DecorateScript(os.Stdin, sb)
	fmt.Println(sb.String())
}

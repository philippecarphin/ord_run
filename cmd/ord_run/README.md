# Ord Run : Un outil pour rouler de jobs

Un outil pour rouler des jobs avec ord_soumet.

Cet outil est un exécutable bâti sur du code écrit pour construire l'exécuteur ordsoumet pour le gitlab-runner.

![Demo de ord_run_helper](ord_run.gif)*Demo de ord_run*

## Références

- https://ecp-ci.gitlab.io/
- https://ecp-ci.gitlab.io/docs/admin/fork/runner-batch.html#scheduler-behavior
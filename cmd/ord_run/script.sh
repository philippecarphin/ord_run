#!/bin/bash

# Demo of some commands
hostname
ifconfig

# We see output as it happens
sleep 3
ls
# The exit code of this script is reported
exit 42

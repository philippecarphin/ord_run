// Command `ord_run` is to `ord_soumet` what `llrun` is to `llsub`
//
// The command takes bash code on `stdin` and parameters for `ord_soumet` as
// its regular parameters.
//
// It can be used to quickly run commands that require lots of resources and
// lets us see their output:
//   
//   echo 'mkdir build; cd build; cmake ..
//   > make -j $(nproc)' | ord_run -c 40 -cm 160g -tmpfs 80g
//
// See the documentation for ordjob package for more details.
package main

import (
	"fmt"
	"io"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"gitlab.science.gc.ca/phc001/ord_run/executor"
	"gitlab.science.gc.ca/phc001/ord_run/gitlab"
)

type programOptions struct {
	keepTmpFiles     bool
	hideCommands     bool
	tmpdir           string
	generateTmpFiles bool
	annotate         bool
	verbosity        int
}

func isatty(f *os.File) bool {
	fileInfo, err := f.Stat()
	if err != nil {
		panic(err)
	}
	return fileInfo.Mode()&os.ModeCharDevice != 0
}

func main() {
	// yamlMain()
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, syscall.SIGINT)

	// _, po := parseCommandLine()

	// The outside of this module provides us with
	// the text of a script
	var rd io.Reader
	var soumetArgs []string
	rd = os.Stdin
	soumetArgs = os.Args[1:]
	sb := strings.Builder{}
	gitlab.DecorateScript(rd, &sb)
	gitlabUserCode := sb.String()

	// We then wrap this code inside a JgenJob
	cmd := executor.NewExecutorCommand(gitlabUserCode, nil)
	cmd.KeepTmpFiles = false
	cmd.BuildTrace = os.Stdout
	if len(os.Args) >= 2 {
		cmd.SoumetStringArgs = strings.Join(soumetArgs, " ")
	} else {
		fmt.Println("Warning : NO arguments provided please try ord_run_helper")
	}

	waitCh := make(chan int)
	go func() {
		exitCode, _ := cmd.Run()
		waitCh <- exitCode
	}()

	var jobStatus int
	select {
	case <-signalCh:
		cmd.AbortCh <- 8
		jobStatus = <-waitCh
	case jobStatus = <-waitCh:
	}

	os.Exit(jobStatus)
}

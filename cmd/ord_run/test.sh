#!/bin/bash
go build

./ord_run -c4 -cm 8g -tmpfs 4g << EOF
hostname
git diff --color=always | head -n 8
sleep 3
declare -x | sort | head -n 8
exit 88
EOF

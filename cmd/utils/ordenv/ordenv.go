package ordenv

import (
	"log"
	"os"

	"gitlab.science.gc.ca/phc001/cmcprompt/cmcprofile"
)

func EnsureOrdenv() {
	ordenvSetup := os.Getenv("ORDENV_SETUP")
	if ordenvSetup == "" {
		log.Printf("Ordenv not loaded, acquiring ordenv environment artificially using gitlab.science.gc.ca/phc001/cmcprofile/cmcprofile\n")
		cmcprofile.AcquireOrdenvEnvironment("1.11.0")
		log.Printf("ordenv environment acquisition done\n")
	}
}

package logging

import (
	"log"
	"os"
)

func configureFileLogging(file string) *os.File {
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	log.Printf("Redirecting logging to file %s for interactive process\n", file)
	log.SetOutput(f)
	return f
}

# ord_run package
This package was developed for the [[https://docs.gitlab.com/runner/][gitlab-runner]] to create the `ordsoumet` executor.

Having this code, we can then make some cool tools:
![Demo of ord_soumet_helper](cmd/ord_soumet_helper/ord_soumet_helper.gif)*Demo of ord_soumet_helper*

- [`ord_soumet_helper`](/cmd/ord_soumet_helper) is shell utility with tooltip autocompletion of ord_soumet
  options with help text.
- [`ord_run`](/cmd/ord_run) can run jobs for us.  It imitates something like `llrun`.
- [`ord_run_helper`](/cmd/ord_run_helper) is like [ord_soumet_helper] but with the tooltips.

## References

Godoc server for this package's documentation
- http://eccc-ppp4:5050/pkg/gitlab.science.gc.ca/phc001/ord_run/

Godoc server for the gitlab runner's ordsoumet executor.
- http://eccc-ppp4:5050/pkg/gitlab.science.gc.ca/aug000/gitlab-runner/executors/ordsoumet/

Wiki page for quick reference on registering a runner.
- https://wiki.cmc.ec.gc.ca/wiki/Gitlab_CI_au_CMC

Gitlab.com documentation on the gitlab runner.
- https://docs.gitlab.com/runner/

American laboratory ECP's documentation about doing CI in an HPC center.
- https://ecp-ci.gitlab.io/

Section of ECP's documentation describign the gist of how the executor works.
- https://ecp-ci.gitlab.io/docs/admin/fork/runner-batch.html#scheduler-behavior
// Package executor is used to test this package, generate an executable and mock how gitlab-runner uses this package
package executor

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"gitlab.science.gc.ca/phc001/ord_run/ordjob"
	"gitlab.science.gc.ca/phc001/ord_run/ordparams"
)

func logger(msg string) {
	log.Printf("\x1b[33m%s\x1b[0m", msg)
}

type ExecutorCommand struct {
	buildScript      string
	BuildTrace       io.Writer
	soumetArgs       []ordparams.OrdSoumetArg
	SoumetStringArgs string
	JobStatus        int
	AbortCh          chan int
	KeepTmpFiles     bool
}

// NewExecutorCommand returns an initialized executor command
func NewExecutorCommand(buildScript string, soumetArgs []ordparams.OrdSoumetArg) *ExecutorCommand {
	e := ExecutorCommand{buildScript: buildScript, soumetArgs: soumetArgs}
	e.AbortCh = make(chan int)
	e.KeepTmpFiles = false
	return &e
}

// Run wraps the execution of a jobinterface.IJob
//
// The command creates an ordjob.OrdSoumetJob.  The function starts it
// and asynchronouly waits for it by calling the QueueStatus() interface
// function.
//
// Whne the function reports that the job is not running, we attempt to
// obtain the exit status of the job which should be in a file.
// If the file is not there, it means the job terminated abnormally and
//
// the wrapper script did not have the chance to write the exit status
// of the wrapped script in the file
//
// To keep things simple, an the int 77 signifies that the job terminated
// abnormally.
//
// The real executor in the gitlab-runner code does something a bit more
// sophisticated using types and reflection.
func (e *ExecutorCommand) Run() (int, error) {
	tmpdirPrefix, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	j, err := ordjob.NewOrdSoumetJob(e.buildScript, e.soumetArgs, tmpdirPrefix)
	if err != nil {
		panic(err)
	}
	if !e.KeepTmpFiles {
		defer os.RemoveAll(j.Files.Dir)
	}

	// We connect the output of the job to gitlab output stream
	j.Stderr = e.BuildTrace
	j.Stdout = e.BuildTrace

	// Runs jobsub
	// Tail starts in a goroutine after job is submitted
	// and begins writing to job.Stdout
	// TODO : Fix it so that the job can be configured to take string arguments or a []ordparams.OrdSoumetArg
	logger(fmt.Sprintf("Submitting job with resources :\n%+v", e.soumetArgs))
	if e.SoumetStringArgs != "" {
		j.StartWithArgs(e.SoumetStringArgs)
	} else {
		j.Start()
	}
	logger("job.Start() -> " + j.Jobid)

	jobExitCodeCh := make(chan int)
	go func() {
		for {
			status, err := j.QueueStatus()
			logger(fmt.Sprintf(" (jobid:%s) job.GetStatus() -> %v", j.Jobid, status))
			if err != nil {
				exitCode, err := j.GetJobExitCode()
				if err != nil {
					logger(fmt.Sprintf("ERROR getting status of job"))
					jobExitCodeCh <- 77
				} else if exitCode != 0 {
					jobExitCodeCh <- exitCode
				} else {
					jobExitCodeCh <- exitCode
				}
				return
			}
			time.Sleep(time.Second * 5)
		}
	}()

	var jobExitCode int
	select {
	case jobExitCode = <-jobExitCodeCh:
	case <-e.AbortCh:
		logger(fmt.Sprintf("Abort requested : Deleting job '%s' with jobdel", j.Jobid))
		j.Delete()
		jobExitCode = <-jobExitCodeCh
	}
	return jobExitCode, nil

}

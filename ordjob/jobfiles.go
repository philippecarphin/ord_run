package ordjob

import (
	"fmt"
	"io/ioutil"
	"os"
)

type jobFiles struct {
	Dir        string
	userScript string
	job        string
	output     string
	status     string
	jobsub     string
	jobdel     string
	jobst      string
}

func createFiles(userCode string, tempDirPrefix string) (jobFiles, error) {
	var f jobFiles
	fi, err := os.Stat(tempDirPrefix)
	if err != nil {
		return f, fmt.Errorf("createFiles() : Cannot stat tempDirPrefix '%s'", tempDirPrefix)
	}
	if !fi.IsDir() {
		return f, fmt.Errorf("createFiles() : tempDifPrefix is not a directory : '%s'", tempDirPrefix)
	}

	f.Dir, err = makeTempdir(tempDirPrefix)
	if err != nil {
		return f, err
	}

	outputFile := createOutputFile(f.Dir)
	statusFile := createStatusFile(f.Dir)
	userScript := f.Dir + "/user_script.sh"
	ioutil.WriteFile(userScript, []byte(userCode), 0755)

	userJob := generateUserJob(f.Dir,
		userScript,
		outputFile,
		statusFile)

	f.jobst = createJobstFile(f.Dir)

	f.output = outputFile
	f.status = statusFile
	f.userScript = userScript
	f.job = userJob

	return f, nil
}

func makeTempdir(tempDirPrefix string) (string, error) {
	dir, err := ioutil.TempDir(tempDirPrefix, "ord_run_temp")
	if err != nil {
		return "", err
	}
	return dir, nil
}

func generateUserJob(dir string, userScript string, outputFile string, statusFile string) string {
	userJob := fmt.Sprintf("#!/bin/sh\n%s >> %s 2>&1\necho $? > %s",
		userScript,
		outputFile,
		statusFile)

	filename := dir + "/user_job.sh"
	ioutil.WriteFile(filename, []byte(userJob), 0755)
	return filename
}

func createOutputFile(dir string) string {
	outputFile := dir + "/output.txt"
	os.Create(outputFile)
	return outputFile
}

func createStatusFile(dir string) string {
	statusFile := dir + "/status.txt"
	os.Create(statusFile)
	return statusFile
}

func (f *jobFiles) getStatusFileContent() string {
	text, _ := ioutil.ReadFile(f.status)
	return string(text)
}

func createJobstFile(dir string) string {
	jobstScript := fmt.Sprintf("#!/bin/sh\nset -eo pipefail\njobst -j $1 | grep $1")
	jobst := dir + "/jobst.sh"
	ioutil.WriteFile(jobst, []byte(jobstScript), 0755)
	return jobst
}

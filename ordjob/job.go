// Package ordjob wraps the execution and monitoring of jobs submitted with ordsoumet and their output as it happens.
//
// The text of the script is put in a temporary file <user_script>.
//
// The actual submitted script is essentially
//
//    #!/bin/bash
//    <user_script> > <output_file> 2>&1
//    echo $? > <exit_code_file>
//
// along with the equivalent of a `tail -f` on `<output_file>`.
//
// When the job is submitted, its `jobid` is stored within the object.
//
// The object's `Wait()` method polls the queing system for the jobid.
//
// When it is detected that the job is no longer running, if it terminated
// normally, its exit code will be in `<exit_code_file>` and can be reported
// back to the application.
package ordjob

import (
	"io"
	"os"

	"gitlab.science.gc.ca/phc001/ord_run/ordparams"
)

// OrdSoumetJob wraps a job sent to a queing system and implements
// the IJob interface.
type OrdSoumetJob struct {
	// Code that wants to be run in a job
	userCode string
	// Resources for that job (becomes the JGEN header)
	SoumetArgs []ordparams.OrdSoumetArg

	// Struct wrapping various Files used in the process
	// of submitting and monitoring the job
	Files jobFiles

	// Id of job once it has been sbumitted
	Jobid string

	// Output streams of the job.  The output of the
	// job will be written to these streams when the
	// job runs.
	Stdout io.Writer
	Stderr io.Writer

	// State indicator for when the job is Done
	Done     bool
	ExitCode int
}

// NewJgenJob constructs a JgenJob.  Use the resources argument to set the desired
// resources for the job.
func NewOrdSoumetJob(userCode string, soumetArgs []ordparams.OrdSoumetArg, tempdirPrefix string) (*OrdSoumetJob, error) {
	j := OrdSoumetJob{userCode: userCode, SoumetArgs: soumetArgs}
	j.Stdout = os.Stdout
	j.Stderr = os.Stderr
	files, err := createFiles(j.userCode, tempdirPrefix)
	if err != nil {
		return nil, err
	}
	j.Files = files
	return &j, nil
}

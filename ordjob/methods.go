package ordjob

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"time"

	"gitlab.science.gc.ca/phc001/ord_run/ordparams"
	"gitlab.science.gc.ca/phc001/ord_run/tail"
)

func (j *OrdSoumetJob) Id() (string, error) {
	return j.Jobid, nil
}

// Start submits the job and launches a tail on the output file.  This tail
// goroutine will write to the Stdout of the job.
func (j *OrdSoumetJob) Start() {
	out, _ := submitWithOrdSoumet(j.Files.job, j.SoumetArgs)
	j.Jobid = strings.TrimSpace(string(out))
	go tail.Tail(j.Files.output, &j.Done, j.Stdout)
}

// Logger defines the interface requirements for the
// log parameter of the Wait function.
type Logger interface {
	Infoln(...interface{})
	Errorln(...interface{})
}

// Wait polls a job and sleeps at a prescribed time interval
// printing the status of the job to the output.
func (j *OrdSoumetJob) Wait(pollInterval time.Duration, out Logger) {
	for ; ; time.Sleep(pollInterval) {

		status, err := j.QueueStatus()
		message := fmt.Sprintf(" (jobid:%s) job.GetStatus() -> %v\n", j.Jobid, status)
		out.Infoln([]byte(message))

		if err != nil {
			break
		}
	}
}

// StartWithArgs is a method that I made real quick.
func (j *OrdSoumetJob) StartWithArgs(soumetArgs string) {
	out, _ := submitWithOrdSoumetStringArgs(j.Files.job, soumetArgs)
	j.Jobid = strings.TrimSpace(string(out))
	go tail.Tail(j.Files.output, &j.Done, j.Stdout)
}

// Check : Check if job is running using jobst
func (j *OrdSoumetJob) Check() int {
	cmd := exec.Command(j.Files.jobst, j.Jobid)
	cmd.Stderr = os.Stderr
	cmd.Run()
	exitCode := cmd.ProcessState.ExitCode()
	if exitCode != 0 {
		j.Done = true
	}
	return exitCode
}

// GetStatus : Returns a status corresponding to the status of the job within the queue
func (j *OrdSoumetJob) QueueStatus() (string, error) {

	jobstScript := fmt.Sprintf("#!/bin/sh\njobst -j %s --format csv\nexec 1>/dev/null", j.Jobid)
	cmd := exec.Command("/bin/sh", "-c", jobstScript)
	cmd.Stderr = os.Stderr

	out, err := cmd.Output()
	if err != nil {
		return "", errors.New("Could not get status of job")
	}

	tokens := strings.Split(string(out), ",")
	if len(tokens) < 3 {
		return "NoExist", fmt.Errorf("The job does not exit")
	}
	// The 'R' in '94586475.cs4fe01,gitlab_runner,R,sanl888,eccc_rpndat,,dev_daemon,'
	status := tokens[2]
	return status, nil
}

// Delete : Stop a running job with jobdel
func (j *OrdSoumetJob) Delete() error {
	out, err := deleteWithJobdel(j.Jobid)
	j.Stderr.Write([]byte(out))
	return err
}

// GetJobExitCode returns the exit code of the job.  The value 77 indicates
// that no exit code could be found in the file that should have contained it.
//
// The gitlab runner distinguishes between an unsuccessful job terminating normally
// and a job that terminates by looking at the value of the error.  If non-nil
// error is received, it should report it and ignore the exit code.  Users should
// never see the value 77.
func (j *OrdSoumetJob) GetJobExitCode() (int, error) {

	content := j.Files.getStatusFileContent()
	var exitCode int
	nbRead, _ := fmt.Sscanf(content, "%d", &exitCode)
	if nbRead != 1 {
		return 77, fmt.Errorf("No exit code found for job.")
	}
	return exitCode, nil
}

func deleteWithJobdel(jobid string) (string, error) {
	jobdelScript := fmt.Sprintf("#!/bin/sh\njobdel %s", jobid)
	cmd := exec.Command("/bin/sh", "-c", jobdelScript)
	cmd.Stderr = os.Stderr
	out, _ := cmd.Output()
	// todo report error if command did not work
	return string(out), nil
}

func submitWithOrdSoumet(filename string, soumetArgs []ordparams.OrdSoumetArg) ([]byte, error) {
	jobsubScript := strings.Builder{}
	fmt.Fprintf(&jobsubScript, "#!/bin/sh\nord_soumet %s", filename)
	// from g2cm, don't think it is necessary
	// fmt.Fprintf(&jobsubScript, " -o %s", "SLEEP_BATCH")
	if len(soumetArgs) == 0 {
		fmt.Fprintf(&jobsubScript, " -c 20 -cm 10G -tmpfs 5G -t 300 -mach %s", os.Getenv("ORDENV_TRUEHOST"))
	}
	for _, arg := range soumetArgs {
		if arg.Name == "ORD_SOUMET_ARGS" {
			jobsubScript.WriteString(" ")
			jobsubScript.WriteString(arg.Value)
		} else {
			fmt.Fprintf(&jobsubScript, " %s %s", arg.Text, arg.Value)
		}
	}
	fmt.Fprintf(&jobsubScript, " -jn %s", "PHILIPPE")
	cmd := exec.Command("/bin/sh", "-c", jobsubScript.String())
	cmd.Stderr = os.Stderr
	out, err := cmd.Output()
	if err != nil {
		return nil, err
	}
	return out, err
}
func submitWithOrdSoumetStringArgs(filename string, soumetArgs string) ([]byte, error) {
	ordsoumetScript := strings.Builder{}
	fmt.Fprintf(&ordsoumetScript, "#!/bin/sh\nord_soumet %s %s", filename, soumetArgs)
	cmd := exec.Command("/bin/sh", "-c", ordsoumetScript.String())
	cmd.Stderr = os.Stderr
	out, err := cmd.Output()
	if err != nil {
		return nil, err
	}
	return out, err
}
